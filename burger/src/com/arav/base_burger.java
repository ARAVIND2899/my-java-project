package com.arav;

public class base_burger {
    private String name;
    private String meat;
    private double price;
    private String breadtype;
    private Base_burger_extra  base_burger_extra;

    public base_burger(String name, String meat,  String breadtype) {
        this.name = name;
        this.meat = meat;
        this.base_burger_extra = new Base_burger_extra("carrot","chips","mayonise","cheese");
        this.price = 3.5;
        this.breadtype = breadtype;

    }
    public void addextra1(){
        base_burger_extra.setAddition1price();
    }


    public String getName() {
        return name;
    }

    public String getMeat() {
        return meat;
    }

    public double getPrice() {
        return this.price + base_burger_extra.getResult();
    }

    public String getBreadtype() {
        return breadtype;
    }


}
