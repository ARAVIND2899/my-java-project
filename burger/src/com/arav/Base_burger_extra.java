package com.arav;

public class Base_burger_extra {
    private String addition_name1;
    private String addition_name2;
    private String addition_name3;
    private String addition_name4;

    private double result=0.0;

    public Base_burger_extra(String addition_name1, String addition_name2, String addition_name3, String addition_name4) {
        this.addition_name1 = addition_name1;
        this.addition_name2 = addition_name2;
        this.addition_name3 = addition_name3;
        this.addition_name4 = addition_name4;
    }

    public String getAddition_name1() {
        return addition_name1;
    }

    public String getAddition_name2() {
        return addition_name2;
    }

    public String getAddition_name3() {
        return addition_name3;
    }

    public String getAddition_name4() {
        return addition_name4;
    }

    public void setAddition1price() {
        this.result += 2.4;
    }

    public void setAddition2price() {
        this.result+= 2.5;
    }

    public void setAddition3price() {
        this.result+= 2.6;
    }

    public void setAddition4price() {
        this.result += 2.7;
    }

    public double getResult() {
        return result;
    }
}
