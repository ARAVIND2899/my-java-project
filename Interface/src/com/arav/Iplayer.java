package com.arav;

import java.util.List;

public interface Iplayer {
    List<String> write();
    void read(List<String> list);
}
