package com.arav;

import java.util.ArrayList;
import java.util.List;

public class player implements Iplayer {
    private String playername;
    private double hitpoint;
    private double strength;
    private String wepon;

    public String getPlayername() {
        return playername;
    }

    public void setPlayername(String playername) {
        this.playername = playername;
    }

    public double getHitpoint() {
        return hitpoint;
    }

    public void setHitpoint(double hitpoint) {
        this.hitpoint = hitpoint;
    }

    public double getStrength() {
        return strength;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    public String getWepon() {
        return wepon;
    }

    public void setWepon(String wepon) {
        this.wepon = wepon;
    }

    @Override
    public String toString() {
        return "player{" +
                "playername='" + playername + '\'' +
                ", hitpoint=" + hitpoint +
                ", strength=" + strength +
                ", wepon='" + wepon + '\'' +
                '}';
    }

    public player(String playername, double hitpoint, double strength) {
        this.playername = playername;
        this.hitpoint = hitpoint;
        this.strength = strength;
        this.wepon="sword";
    }

    @Override
    public List<String> write() {
        List<String> values=new ArrayList<String>();
        values.add(0,this.playername);
        values.add(1," "+this.hitpoint);
        values.add(2," "+this.strength);
        values.add(3,this.wepon);

        return values;

    }

    @Override
    public void read(List<String> list) {
        if(list!=null&&list.size()>0) {
            this.playername = list.get(0);
            this.hitpoint = Double.parseDouble(list.get(1));
            this.strength = Double.parseDouble(list.get(2));
            this.wepon = list.get(3);

        }
        System.out.println( toString());

    }
}
