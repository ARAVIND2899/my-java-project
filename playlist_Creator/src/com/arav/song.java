package com.arav;

public class song {
    private String name;
    private double duration;

    public song(String name, double duration) {
        this.name = name;
        this.duration = duration;
    }
    public static song Creatsong(String name, double duration){
        return new song(name,  duration);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return " song of " +
                "name='" + name + '\'' +
                ", duration=" + duration +
                " min";
    }
}
