package com.arav;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class album {
    private  String album_name;
    private String artist_name;
     private songlist songs;

    public String getAlbum_name() {
        return album_name;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public List<song> getSongs() {
        return songs.getSongs();
    }

    public album(String album_name, String artist_name) {
        this.album_name = album_name;
        this.artist_name = artist_name;
        this.songs = new songlist();
    }
    public boolean addsong(String song_name,double duration){
//        if(findsong(song_name)==null){
//           this.songs.add(new song(song_name,duration));
//           return true;
//        }
//        return  false;
        return this.songs.addsong(new song(song_name,duration));
    }
//    private song findsong(String song_name){
//        for(song checkedsong:this.songs){
//            if(checkedsong.getName().equals(song_name)){
//                return checkedsong;
//            }
//        }
//        return null;
//    }
   public boolean addToPlayList(int trackNumber, List<song> playList) {
//        int index = trackNumber -1;
//        if((index >=0) && (index <= this.songs.size())) {
//            playList.add(this.songs.get(index));
//            return true;
//        }
//        System.out.println("This album does not have a track " + trackNumber);
//        return false;
       return songs.addToPlayList(trackNumber,playList);
   }
    public boolean addToPlaylist(String song_name,List<song> playlist){
        return this.songs.addToPlaylist(song_name,playlist);

    }
    public static album Creatalbum(String album_name, String artist_name){
        return new album(album_name,artist_name);
    }
    private class songlist{
         private List<song> list;

        public songlist() {
            this.list = new ArrayList<>();
        }
        private boolean addsong(song song_name){
            if(this.list.contains(song_name)){
                System.out.println("hi");
                return  false;
            }
            list.add(song_name);
            return true;
        }
        private song findsong(String song_name){
            for(song checkedsong:this.list){
                if(checkedsong.getName().equals(song_name)){
                    return checkedsong;
                }
            }
            return null;
        }
        public boolean addToPlaylist(String song_name,List<song> playlist){
            song checkedsong=findsong(song_name);
            if(checkedsong!=null){
                playlist.add(checkedsong);
                return true;
            }
            return false;


        }
        public boolean addToPlayList(int trackNumber, List<song> playList) {
            int index = trackNumber -1;
            if((index >=0) && (index <= this.list.size())) {
                playList.add(this.list.get(index));
                return true;
            }
            System.out.println("This album does not have a track " + trackNumber);
            return false;
        }

        public List<song> getSongs() {
            return this.list;
        }
    }
}
