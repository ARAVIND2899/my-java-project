// Java program to read a matrix from user

import java.util.Scanner;

public class MatrixFromUser {



    // Function to read matrix


    // Driver code
    public static void main(String[] args)
    {
        int m, n, i, j;
        int count=0;
        Scanner in = null;

            in = new Scanner(System.in);
            System.out.println("Enter the number "
                    + "of rows of the matrix");
            m = in.nextInt();
            n=m;

            // Declare the matrix
            int first[][] = new int[m][n];

            // Read the matrix values
            System.out.println("Enter the elements of the matrix");
            for (i = 0; i < m; i++)
                for (j = 0; j < n; j++)
                    first[i][j] = in.nextInt();

            // Display the elements of the matrix
            System.out.println("Elements of the matrix are");
            for (i = 0; i < m; i++) {
                for (j = 0; j < n; j++)
                    System.out.print(first[i][j] + " ");
                System.out.println();
            }
        for (i = 0; i < m; i++) {
            for ( j = 0; j < n; j++) {
                if (i != 0 && i!=m-1&&j!=0&&j!=n-1){
                    System.out.print(first[i][j] + " ");
                    count +=first[i][j];
                }

            }
            System.out.println("");

        }
        System.out.println(count);




    }
}
