import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class sumFinder {
    public static void main(String[] args){
        int i=findSumeetSum(26674,105,37943,95278,27845);
        System.out.println(i);
    }
    static int findSumeetSum(int input1, int input2, int input3, int input4, int input5){

        int result=0;
        int[] intArray = new int[]{ input1,input2,input3,input4,input5};
        for(int i=0;i<intArray.length;i++){
            List<Integer> list1;
            list1=splitter(intArray[i]);
            String str1="";
            for(int z = 0; z < 3; z++)
            {
                str1  += Integer.toString(list1.get(z));
            }

            result  += Integer.parseInt(str1);
        }


        return result;
    }
    static List<Integer> splitter(int num){
        int number=num;
        List<Integer> list2=new ArrayList<Integer>();
        while (number > 0) {
            list2.add(number%10);
            number = number / 10;
        }

        Collections.sort(list2,Collections.reverseOrder());

        return list2;
    }

}
