package com.arav;

public class MyLinkedList implements NodeList {
    private ListItem root=null;

    public MyLinkedList(ListItem root) {
        this.root= root;
    }


    @Override
    public boolean addItem(ListItem newItem) {
        if (this.root == null) {
            this.root = newItem;
            return true;
        }
        ListItem currentItem=this.root;
        while (currentItem!=null){
            int comparison=currentItem.compareTo(newItem);
            if(comparison<0) {
                if (currentItem.next()!=null) {
                    currentItem = currentItem.next();
                }
                else {
                    // there is no next, so insert at end of list
                    currentItem.setnext(newItem).setprev(currentItem);
                    return true;
                }
            }
            else if(comparison >0) {
                // newItem is less, insert before
                if(currentItem.prev() != null) {
                    currentItem.prev().setnext(newItem).setprev(currentItem.prev());
                    newItem.setnext(currentItem).setprev(newItem);
                } else {
                    // the node with a previous is the root
                    newItem.setnext(this.root).setprev(newItem);
                    this.root = newItem;
                }
                return true;
            }
            else {
                System.out.println(newItem.getValue()+"is already here");
                return false;
            }

        }
        return false;
    }



    @Override
    public boolean remove(ListItem item) {
//        if(this.root==null){
//            System.out.println("there is nothing to remove");
//            return false;
//        }
//        ListItem currentirm=listItem;
//        while (currentirm.next()!=null){
//            currentirm=currentirm.next();
//        }
//        currentirm.prev().setnext(null);
//        return true;
//    }
        if(item != null) {
            System.out.println("Deleting item " + item.getValue());
        }

        ListItem currentItem = this.root;
        while(currentItem != null) {
            int comparison = currentItem.compareTo(item);
            if(comparison == 0) {
                // found the item to delete
                if(currentItem == this.root) {
                    this.root = currentItem.next();
                } else {
                    currentItem.prev().setnext(currentItem.next());
                    if(currentItem.next() != null) {
                        currentItem.next().setprev(currentItem.prev());
                    }
                }
                return true;
            } else if(comparison <0) {
                currentItem = currentItem.next();
            } else { // comparison > 0
                // We are at an item greater than the one to be deleted
                // so the item is not in the list
                return false;
            }
        }

        // We have reached the end of the list
        // Without finding the item to delete
        return false;
    }

    @Override
    public void traverse(ListItem root) {
         if(root==null){
             System.out.println("the root value is null");
         }
         while (root!=null){
             System.out.println(root.getValue()+"is added");
             root=root.next();
         }
    }

    @Override
    public ListItem getroot() {
        return this.root;
    }
}
