package com.arav;

public class Node extends ListItem {


    public Node(Object value) {
        super(value);
    }

    @Override
    ListItem next() {
        return this.right;
    }

    @Override
    ListItem prev() {
        return this.left;
    }

    @Override
    ListItem setnext(ListItem listItem) {
        this.right=listItem;
        return this.right;

    }

    @Override
    ListItem setprev(ListItem listItem) {
        this.left=listItem;
        return this.left;
    }

    @Override
    int compareTo(ListItem listItem) {
        if(listItem!=null){
            return ((String)super.getValue()).compareTo((String)listItem.getValue());
        }
        return -1;
    }
}
