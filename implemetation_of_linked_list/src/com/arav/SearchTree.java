package com.arav;

public class SearchTree  implements NodeList{

   private ListItem root=null;

    public SearchTree(ListItem root) {
        this.root = root;
    }

    @Override
    public boolean addItem(ListItem newitem) {
        if(this.root==null){
            this.root=newitem;
            return true;
        }
        ListItem currentitem=this.root;
        while (currentitem!=null){
            int comparison=currentitem.compareTo(newitem);
            if(comparison <0){
                if(currentitem.next()!=null){
                    currentitem=currentitem.next();
                }
                else {
                    currentitem.setnext(newitem);
                    return true;
                }

            }
            else if(comparison>0){
                if(currentitem.prev()!=null){
                    currentitem=currentitem.prev();

                }
                else {
                    currentitem.setprev(newitem);
                    return true;
                }
            }
            else {
                System.out.println("it is already in the list");
                return false;
            }
        }
        return false;
    }
    private void performremove(ListItem item, ListItem parent) {
        // remove item from the tree
        if (item.next() == null) {
            // no right tree, so make parent point to left tree (which may be null)
            if (parent.next() == item) {
                // item is right child of its parent
                parent.setnext(item.prev());
            } else if (parent.prev() == item) {
                // item is left child of its parent
                parent.setprev(item.prev());
            } else {
                // parent must be item, which means we were looking at the root of the tree
                this.root = item.prev();
            }
        } else if (item.prev() == null) {
            // no left tree, so make parent point to right tree (which may be null)
            if (parent.next() == item) {
                // item is right child of its parent
                parent.setnext(item.next());
            } else if (parent.prev() == item) {
                // item is left child of its parent
                parent.setprev(item.next());
            } else {
                // again, we are deleting the root
                this.root = item.next();
            }
        } else {
            // neither left nor right are null, deletion is now a lot trickier!
            // From the right sub-tree, find the smallest value (i.e., the leftmost).
            ListItem current = item.next();
            ListItem leftmostParent = item;
            while (current.prev() != null) {
                leftmostParent = current;
                current = current.prev();
            }
            // Now put the smallest value into our node to be deleted
            item.setValue(current.getValue());
            // and delete the smallest
            if (leftmostParent == item) {
                // there was no leftmost node, so 'current' points to the smallest
                // node (the one that must now be deleted).
                item.setnext(current.next());
            } else {
                // set the smallest node's parent to point to
                // the smallest node's right child (which may be null).
                leftmostParent.setprev(current.next());
            }
        }
    }
    @Override
    public boolean remove(ListItem listItem) {
        if (listItem != null) {
            System.out.println("Deleting item " + listItem.getValue());
        }
        ListItem currentItem = this.root;
        ListItem parentItem = currentItem;

        while (currentItem != null) {
           int coparison =(currentItem.compareTo(listItem));
           if(coparison<0){
               parentItem=currentItem;
               currentItem=currentItem.next();
           }else if(coparison>0){
               parentItem=currentItem;
               currentItem=currentItem.prev();
           }else {
               performremove(parentItem,currentItem);
               return true;
           }

    }
        return false;

}



    @Override
    public void traverse(ListItem root) {
        if (root != null) {
            traverse(root.prev());
            System.out.println(root.getValue());
            traverse(root.next());
        }


    }

    @Override
    public ListItem getroot() {
        return this.root;
    }
}
