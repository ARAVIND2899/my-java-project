package com.arav;

public abstract  class ListItem {
    protected ListItem right=null;
    protected ListItem left=null;
    private Object value;

    public ListItem(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
    abstract ListItem next();
    abstract ListItem prev();
    abstract ListItem setnext(ListItem listItem);
    abstract ListItem setprev(ListItem listItem);
    abstract int compareTo(ListItem listItem);
}
