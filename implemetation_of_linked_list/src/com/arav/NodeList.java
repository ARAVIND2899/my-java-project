package com.arav;

public interface NodeList {
    boolean addItem(ListItem listItem);
    boolean remove(ListItem listItem);
    void traverse(ListItem root);
    ListItem getroot();
}
