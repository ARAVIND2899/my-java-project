package com.arav;

public class Department {
    private String dept_name;
    private String dept_est;
    private String Hod_name;

    public Department(String dept_name, String dept_est, String hod_name) {
        this.dept_name = dept_name;
        this.dept_est = dept_est;
        Hod_name = hod_name;
    }

    public String getDept_name() {
        return dept_name;
    }

    @Override
    public String toString() {
        return "Department{" +
                "dept_name='" + dept_name + '\'' +
                ", dept_est='" + dept_est + '\'' +
                ", Hod_name='" + Hod_name + '\'' +
                '}';
    }
}
