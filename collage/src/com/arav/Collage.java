package com.arav;

import java.util.ArrayList;

public class Collage {
    private   String colllage_Name;
    private String Year_Of_Est;
    private ArrayList<Department> departments;

    public Collage(String colllage_Name, String year_Of_Est) {
        this.colllage_Name = colllage_Name;
        Year_Of_Est = year_Of_Est;
        this.departments = new ArrayList<>();
    }
    public boolean adddept(Department department1){
       if(!finddept(department1)) {
           System.out.println("false");
           this.departments.add(department1);

           return true;
       }
        System.out.println("it is already there");
       return false;
    }
    public int indesof(Department department){
        return this.departments.indexOf(department);
    }

    private boolean finddept(Department department1) {
        for(Department dept:this.departments){
            if(dept.getDept_name().equals(department1.getDept_name())){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Collage{" +
                "colllage_Name='" + colllage_Name + '\'' +
                ", Year_Of_Est='" + Year_Of_Est + '\'' +
                ", departments=" + departments +
                '}';
    }
}
