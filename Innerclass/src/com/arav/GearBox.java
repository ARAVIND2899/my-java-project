package com.arav;
import java.util.*;
public class GearBox {
    private int maxgear;
    private int current_gear=0;
    private boolean cluchin;
    private List<Gear> gears;

    public GearBox(int maxgear) {
        this.maxgear = maxgear;
        this.gears = new ArrayList<>();
        Gear neutral=new Gear(0,0.0);
        //this.gears.add(neutral);
        for(int i=0;i<this.maxgear;i++){
            addgear(i,i*5.3);
        }
    }

    public void addgear(int gear, double ratio){
        if((gear>0)&&(gear<this.maxgear)){
            gears.add(new Gear(gear,ratio));

        }
    }
    public void operateclutch(boolean in){
        this.cluchin=in;
    }
    public void changegear(int newgear){
        if((newgear>=0)&&(newgear<this.gears.size())&&(this.cluchin)){
            this.current_gear=newgear;
            System.out.println("the gear is changed "+current_gear);

        }
        else {
            System.out.println("grind");
            this.current_gear=0;
        }
    }
    public double wheelspeed(int revs){
        if(cluchin){
            System.out.println("scream");

            return 0.0;
        }

        return gears.get(current_gear).drivespeed(revs);
    }
    private class Gear{
        private int gearnumber;
        private double gearratio;

        public Gear(int gearnumber, double gearratio) {
            this.gearnumber = gearnumber;
            this.gearratio = gearratio;

            
        }
        public  double drivespeed(int revs){
            return revs*(getGearratio());
        }

        public double getGearratio() {
            return gearratio;
        }
    }
}
