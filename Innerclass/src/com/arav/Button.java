package com.arav;

public class Button {
    private String btn_name;
     private onclicklistenr onclickListenr;
//    private overloded overloded1;

//    public void setOverloded1(overloded overloded1) {
//        this.overloded1 = overloded1;
//    }
//    public void overloded(){
//        this.overloded1.overloaded();
//    }

    public Button(String btn_name) {
        this.btn_name = btn_name;
    }
    public void setOnclickListenr(onclicklistenr onclickListenr){
        this.onclickListenr=onclickListenr;
    }
    public  void onClick(){
        this.onclickListenr.onclick(this.btn_name);
    }
    public interface onclicklistenr{
        void onclick(String title);
    }
//    public interface overloded{
//        void overloaded();
//    }

}
