package com.arav;

import java.util.ArrayList;

public class Banks {
    private String name;
    private ArrayList<Branches> branches;
    private static Branches branches1;

    public Banks(String name) {
        this.name = name;
        this.branches = new ArrayList<Branches>();
    }
    private Branches findBranch(String branchname){
        for( int i=0;i<branches.size();i++){
            branches1=this.branches.get(i);
            if(branches1.getName().equals(branchname)){
                return branches1;
            }
        }
        return null;
    }
    public boolean addBranch(String branchName) {
        branches1=findBranch(branchName);
        if( branches1== null) {
            this.branches.add(new Branches(branchName));
            return true;
        }

        return false;
    }
    public boolean addCustomer(String branchname,String customername,Double initial_amount){
        branches1=findBranch(branchname);
        if(branches1!=null){
             return branches1.addCustomer(customername,initial_amount);
        }
        return false;
    }
    public boolean addCustomerTransaction(String branchName, String customerName, double amount) {
        Branches branch = findBranch(branchName);
        if(branch != null) {
            return branch.addCustomerTransaction(customerName, amount);
        }

        return false;
    }
    public boolean listCustomers(String branchName, boolean showTransactions) {
        Branches branch = findBranch(branchName);
        if (branch != null) {
            System.out.println("Customer details for branch " + branch.getName());

            ArrayList<Customers> branchCustomers = branch.getCustomers();
            for (int i = 0; i < branchCustomers.size(); i++) {
                Customers branchCustomer = branchCustomers.get(i);
                System.out.println("Customer: " + branchCustomer.getName() + "[" + (i + 1) + "]");
                if (showTransactions) {
                    System.out.println("Transactions");
                    ArrayList<Double> transactions = branchCustomer.getTransactions();
                    for (int j = 0; j < transactions.size(); j++) {
                        System.out.println("[" + (j + 1) + "]  Amount " + transactions.get(j));
                    }
                }
                System.out.println("totla money"+branchCustomer.getTotalmoney());
            }

            return true;
        } else {
            return false;
        }
    }

}
