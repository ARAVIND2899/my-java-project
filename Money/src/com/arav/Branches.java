package com.arav;

import java.util.ArrayList;

public class Branches {
    private String name;
    private ArrayList<Customers> Customers;
    private static Customers customers;

    public Branches(String name) {
        this.name = name;
        this.Customers=new ArrayList<Customers>();
    }

    public String getName() {
        return name;
    }


    public ArrayList<Customers> getCustomers() {
        return Customers;
    }
    private Customers findcustomer(String customername){
        for( int i=0;i<Customers.size();i++){
            customers=this.Customers.get(i);
            if(customers.getName().equals(customername)){
                return customers;
            }
        }
        return null;
    }
    public boolean addCustomer(String name,double initianl_amount){
        if(findcustomer(name)==null){
            Customers.add(new Customers(name,initianl_amount));
            return true;
        }
        return false;
    }
    public boolean addCustomerTransaction(String customerName, double amount) {
        customers = findcustomer(customerName);
        if(customers != null) {
            customers.addTransction(amount);
            return true;
        }

        return false;
    }

}
