package com.arav;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Banks banks=new Banks("STATE BANK OF INDIA");
        boolean value=banks.addBranch("K.PUDUR");
        if(value){
            System.out.println("branch is created");
        }
        value=banks.addCustomer("K.PUDUR","Arav",300.00);
        System.out.println(value);
        banks.listCustomers("K.PUDUR",true);
        banks.addCustomerTransaction("K.PUDUR","Arav",300);
        banks.listCustomers("K.PUDUR",true);

    }
}
