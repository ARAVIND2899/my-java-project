package com.arav;

import java.util.ArrayList;

public class Customers {
    private String name;
    private ArrayList<Double> transactions;
    private double totalmoney;

    public Customers(String name,double transaction) {
        this.name = name;
        this.transactions=new ArrayList<Double>();

        addTransction(transaction);
    }

    public double getTotalmoney() {
        return totalmoney;
    }

    public void addTransction(double transaction) {
        this.transactions.add(transaction);
        totalmoney+=transaction;

    }

    public String getName() {
        return name;
    }

    public ArrayList<Double> getTransactions() {
        return transactions;
    }
}
