package com.arav;

import java.util.ArrayList;
import java.util.Deque;

public class Team<T extends Player> implements Comparable<Team<T>> {
    private String team_name;
    int played = 0;
    int won = 0;
    int lost = 0;
    int tied = 0;

    public Team(String team_name) {
        this.team_name = team_name;
    }

    private ArrayList<T> members=new ArrayList();
    public boolean addPlayer(T player) {
        if (members.contains(player)) {
            System.out.println(player.getPlayername() + " is already on this team");
            return false;
        } else {
            members.add(player);
            System.out.println(player.getPlayername() + " picked for team " + this.team_name);
            return true;
        }
    }
    public int numPlayers() {
        return this.members.size();
    }
    public int rankings(){

        return (won*2)+tied;
    }
    public void matchresult(Team<T> opponent,int ours,int theirs){
        String message;
        if(ours>theirs){
            won++;
            message=" beat ";
        }else if(theirs>ours){
            lost--;
            message=" lost ";
        }
        else {
            tied++;
            message=" tied ";
        }
        if(opponent!=null){
            System.out.println(this.getTeam_name() + message + opponent.getTeam_name());
            opponent.matchresult(null,theirs,ours);
        }
    }

    public String getTeam_name() {
        return team_name;
    }

   //@Override
    public int compareTo(Team<T> team) {
        if(this.rankings()>team.rankings()){

            return -1;
        }
        if(this.rankings()< team.rankings()){

            return 1;
        }
        return 0;
    }
}
