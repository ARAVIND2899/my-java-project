package com.arav;

public abstract class Player {
    private final String playername;

    public Player(String playername) {
        this.playername = playername;
    }

    public String getPlayername() {
        return playername;
    }

}
