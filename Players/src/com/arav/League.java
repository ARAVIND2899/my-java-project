package com.arav;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class League<T extends Team>   {
    private String LeagueName;
    ArrayList<T> teams=new ArrayList<>();;

    public League(String leagueName) {
        LeagueName = leagueName;

    } public  boolean Addteam(T team){
        if(!this.teams.contains(team)){
            this.teams.add(team);
            return true;
        }
        return false;

    }
    public void  showstandings(){

        Collections.sort(this.teams);
        for (T t:this.teams){
            System.out.println(t.getTeam_name() +" with points "+ t.rankings());
        }
    }



}
